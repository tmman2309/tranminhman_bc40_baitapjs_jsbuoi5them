// ex1
function ex1TinhTienThue(){
    var hoTen = document.getElementById("ex1-hoten").value;
    var tongThuNhap = document.getElementById("ex1-tongthunhap").value *1;
    var soNguoi = document.getElementById("ex1-songuoi").value *1;
    var thongBao = document.getElementById("ex1_thongbao");

    var thuNhapChiuThue = tongThuNhap - 4000000 - soNguoi*1600000;
    var thue = 0;
    if(hoTen==""){
        alert("Vui lòng nhập họ tên");
        return;
    };
    if(tongThuNhap<= 6e+7){
        thue = thuNhapChiuThue*0.05;
    } else if(tongThuNhap <=12e+7){
        thue = thuNhapChiuThue*0.1;
    } else if(tongThuNhap <=21e+7){
        thue = thuNhapChiuThue*0.15;
    } else if(tongThuNhap <=384e+6){
        thue = thuNhapChiuThue*0.2;
    } else if(tongThuNhap <=624e+6){
        thue = thuNhapChiuThue*0.25
    } else if(tongThuNhap <=96e+7){
        thue = thuNhapChiuThue*0.3;
    } else{
        thue = thuNhapChiuThue*0.35;
    };

    if(thue<0){
        alert("Số tiền thu nhập không hợp lệ! Vui lòng nhập lại!")
    } else{
        thongBao.innerText = `👉 Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${new Intl.NumberFormat('vn-VN', {style: 'currency', currency: 'VND'}).format(thue)}`;
    };
};

// ex2
var soKetNoiEl = document.getElementById("ex2-soketnoi");
soKetNoiEl.style.display="none";

function ex2onChange(){
    var loaiKH = document.getElementById("ex2-loaikh").value;
    if(loaiKH == "dn"){
        soKetNoiEl.style.display="block";
    } else{
        soKetNoiEl.style.display="none";
    }
}

function ex2TinhTienCap(){
    var loaiKH = document.getElementById("ex2-loaikh").value;
    var maKH = document.getElementById("ex2-makh").value;
    var soKenh = document.getElementById("ex2-sokenh").value *1;
    var soKetNoi = soKetNoiEl.value *1;
    var thongBao = document.getElementById("ex2_thongbao");
    var tongTien =0;

    if(loaiKH == "df"){
        alert("Vui lòng chọn loại khách hàng!")
        return;
    } else if(loaiKH == "nd"){
        tongTien = 4.5 + 20.5 + 7.5*soKenh;
    } else if(loaiKH == "dn"){
        if(soKetNoi <= 0 || soKetNoi ==""){
            alert("Vui lòng nhập lại số kết nối!");
            return;
        } else if(soKetNoi <=10){
            tongTien = 15 + 75 + 50*soKenh;
        } else{
            tongTien = 15 + 75 + 50*soKenh + 5*(soKetNoi-10);
        }
    };

    if(maKH == ""){
        alert("Vui lòng nhập lại mã khách hàng!");
        return;
    };
    if(soKenh <= 0 || soKenh == ""){
        alert("Vui lòng nhập lại số kênh!");
        return;
    };

    thongBao.innerText = `👉 Mã khách hàng: ${maKH}; Tiền cáp: $${new Intl.NumberFormat('en-IN').format(tongTien)}`; 
}